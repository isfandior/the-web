<?php
/*
    Template Name: Prices Page
*/

get_header(); 

?>
    <!--====== Breadcumb Section start ======-->
    <section class="banner-section banner-section-three">
        <div class="banner-slider">
            <div class="single-banner">
                <div class="container-fluid container-1600">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <h2 class="wow fadeInLeft breadcumb-title">Prices</h2>
                            <ul class="breadcumb-list">
                                <li class="breadcumb-list_item"><a href="/">Home</a></li>
                                <li class="breadcumb-list_item">Prices</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="banner-shapes">
                    <div class="one"></div>
                    <div class="two"></div>
                    <div class="three"></div>
                </div>
                <div class="banner-line">
                    <img src="<?=get_template_directory_uri()?>/assets/img/lines/17.png" alt="Image">
                </div>
            </div>
        </div>
    </section>
    <!--====== Breadcumb Section end ======-->

    <!--====== Prices Section Start ======-->
    <section class="faq-section section-gap">
        <div class="container">
            <!-- Prices Page Title -->
            <div class="row faq-page-title mb-60 align-items-center">
                <div class="col-lg-6">
                    <div class="section-title left-border">
                        <span class="title-tag">Prices</span>
                        <h2 class="title">Prices Of Our Services</h2>
                    </div>
                </div>
                <div class="col-lg-6">
                    <p>
                        But must explain to you how all this mistaken idea of denounc easure and praising pain was born
                        and I will give you a compl the system, and expound the actual teachings of the great explorer
                        the truth,
                    </p>
                </div>
            </div>

            <!-- Prices LOOP -->
            <div class="accordion faq-loop grey-header" id="faqAccordion">
                <div class="card">
                    <div class="card-header">
                        <h6 class="collapsed" data-toggle="collapse" data-target="#collapseOne">
                            Prices Of Creating Website
                            <span class="icons">
                                <i class="far fa-plus"></i>
                            </span>
                        </h6>
                    </div>

                    <div id="collapseOne" class="collapse" data-parent="#faqAccordion">
                        <div class="card-body">
                            <!--====== Price Section Start ======-->
                            <section class="prices-section mt-5">
                                <div class="container">
                                    <div class="prices__header">
                                        <div class="container section-title">
                                            <h2 class="title">Our Prices </h2>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Intro</h4>
                                                <h3 class="prices mt-3">$19 <span class="prices__period"></span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Base</h4>
                                                <h3 class="prices mt-3">$39 <span class="prices__period"></span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Popular</h4>
                                                <h3 class="prices mt-3">$99 <span class="prices__period"></span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!--====== Price Section end ======-->
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header active-header">
                        <h6 class="collapsed" data-toggle="collapse" data-target="#collapseTwo">
                            Prices Of Creating eCommerce Website
                            <span class="icons">
                                <i class="far fa-minus"></i>
                            </span>
                        </h6>
                    </div>

                    <div id="collapseTwo" class="collapse show" data-parent="#faqAccordion">
                        <div class="card-body">
                            <!--====== Price Section Start ======-->
                            <section class="prices-section mt-5">
                                <div class="container">
                                    <div class="prices__header">
                                        <div class="container section-title">
                                            <h2 class="title">Our Prices </h2>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Intro</h4>
                                                <h3 class="prices mt-3">$19 <span class="prices__period"></span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Base</h4>
                                                <h3 class="prices mt-3">$39 <span class="prices__period"></span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Popular</h4>
                                                <h3 class="prices mt-3">$99 <span class="prices__period"></span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!--====== Price Section end ======-->
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h6 class="collapsed" data-toggle="collapse" data-target="#collapseThree">
                            Prices Of Search Engine Optimization [SEO]
                            <span class="icons">
                                <i class="far fa-plus"></i>
                            </span>
                        </h6>
                    </div>

                    <div id="collapseThree" class="collapse" data-parent="#faqAccordion">
                        <div class="card-body">
                            <!--====== Price Section Start ======-->
                            <section class="prices-section mt-5">
                                <div class="container">
                                    <div class="prices__header">
                                        <div class="container section-title">
                                            <h2 class="title">Our Prices </h2>
                                        </div>
                                        <div id="myBtnContainer">
                                            <div class="btn-container">
                                              <button class="filter-btn active" onclick="filterSelection('month')"> Monthly</button>
                                              <button class="filter-btn" onclick="filterSelection('year')"> Yearly</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 filterDiv month">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Intro</h4>
                                                <h3 class="prices mt-3">$19 <span class="prices__period">/monthly</span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 filterDiv month">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Base</h4>
                                                <h3 class="prices mt-3">$39 <span class="prices__period">/monthly</span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 filterDiv month">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Popular</h4>
                                                <h3 class="prices mt-3">$99 <span class="prices__period">/monthly</span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 filterDiv year">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Intro</h4>
                                                <h3 class="prices mt-3">$19 <span class="prices__period">/yearly</span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 filterDiv year">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Base</h4>
                                                <h3 class="prices mt-3">$39 <span class="prices__period">/yearly</span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 filterDiv year">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Popular</h4>
                                                <h3 class="prices mt-3">$99 <span class="prices__period">/yearly</span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!--====== Price Section end ======-->
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h6 class="collapsed" data-toggle="collapse" data-target="#collapseFour">
                            Prices Of Social Media Marketing [SMM]
                            <span class="icons">
                                <i class="far fa-plus"></i>
                            </span>
                        </h6>
                    </div>

                    <div id="collapseFour" class="collapse" data-parent="#faqAccordion">
                        <div class="card-body">
                            <!--====== Price Section Start ======-->
                            <section class="prices-section mt-5">
                                <div class="container">
                                    <div class="prices__header">
                                        <div class="container section-title">
                                            <h2 class="title">Our Prices </h2>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Intro</h4>
                                                <h3 class="prices mt-3">$19 <span class="prices__period"></span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Base</h4>
                                                <h3 class="prices mt-3">$39 <span class="prices__period"></span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Popular</h4>
                                                <h3 class="prices mt-3">$99 <span class="prices__period"></span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!--====== Price Section end ======-->
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h6 class="collapsed" data-toggle="collapse" data-target="#collapseFive">
                            Prices Of Google Adwords [Ad]
                            <span class="icons">
                                <i class="far fa-plus"></i>
                            </span>
                        </h6>
                    </div>

                    <div id="collapseFive" class="collapse" data-parent="#faqAccordion">
                        <div class="card-body">
                            <!--====== Price Section Start ======-->
                            <section class="prices-section mt-5">
                                <div class="container">
                                    <div class="prices__header">
                                        <div class="container section-title">
                                            <h2 class="title">Our Prices </h2>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Intro</h4>
                                                <h3 class="prices mt-3">$19 <span class="prices__period"></span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Base</h4>
                                                <h3 class="prices mt-3">$39 <span class="prices__period"></span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Popular</h4>
                                                <h3 class="prices mt-3">$99 <span class="prices__period"></span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!--====== Price Section end ======-->
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h6 class="collapsed" data-toggle="collapse" data-target="#collapseSix">
                            Prices Of Creating Telegram Bots
                            <span class="icons">
                                <i class="far fa-plus"></i>
                            </span>
                        </h6>
                    </div>

                    <div id="collapseSix" class="collapse" data-parent="#faqAccordion">
                        <div class="card-body">
                            <!--====== Price Section Start ======-->
                            <section class="prices-section mt-5">
                                <div class="container">
                                    <div class="prices__header">
                                        <div class="container section-title">
                                            <h2 class="title">Our Prices </h2>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Intro</h4>
                                                <h3 class="prices mt-3">$19 <span class="prices__period"></span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Base</h4>
                                                <h3 class="prices mt-3">$39 <span class="prices__period"></span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6">
                                            <div class="prices__block">
                                                <h4 class="prices__title">Popular</h4>
                                                <h3 class="prices mt-3">$99 <span class="prices__period"></span></h3>
                                                <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                                                <ul class="mt-3 prices__list-parent">
                                                    <li class="prices__list">All limited links</li>
                                                    <li class="prices__list">Own analytics platform</li>
                                                    <li class="prices__list">Chat Support</li>
                                                    <li class="prices__list">Optimize Hashtags</li>
                                                    <li class="prices__list">Unlimited Users</li>
                                                </ul>
                                                <a class="main-btn mt-5" href="#">Choose Plan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!--====== Price Section end ======-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Faq LOOP -->
        </div>
    </section>
    <!--====== Prices Section End ======-->

    <!--====== How We Work Section Start ======-->
    <section class="skill-section">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-6 col-md-10">
                    <!-- Skill Text Block -->
                    <div class="skill-text">
                        <div class="section-title mb-40 left-border">
                            <span class="title-tag">How We Work ?</span>
                            <h2 class="title">We Have Experience <br> For Business Planning</h2>
                        </div>
                        <p>
                            Sedut perspiciatis unde omnis iste natus error sit voluptat em accusantium doloremque
                            laudantium, totam raperiaeaque ipsa quae ab illo inventore veritatis et quasi
                        </p>
                        <p>
                            But I must explain to you how all this mistaken idenouncing pleasure and praising pain was
                            born and I will give completey account of the system, and expound the actual teachings of
                            the great explorer of the truth, the master-builder of human happiness one rejects,
                            dislikes, or avoid
                        </p>

                        <a href="#" class="main-btn">Learn More</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-10">
                    <div class="piechart-boxes">
                        <div class="chart-box">
                            <div class="chart" data-percent="25">
                                <i class="flaticon-presentation"></i>
                            </div>
                            <h4 class="title">Business Strategy</h4>
                        </div>
                        <div class="chart-box">
                            <div class="chart" data-percent="40">
                                <i class="flaticon-money-bags"></i>
                            </div>
                            <h4 class="title">Financial Planing</h4>
                        </div>
                        <div class="chart-box">
                            <div class="chart" data-percent="75">
                                <i class="flaticon-invest"></i>
                            </div>
                            <h4 class="title">Marketing Startegy</h4>
                        </div>
                        <div class="chart-box">
                            <div class="chart" data-percent="80">
                                <i class="flaticon-connector"></i>
                            </div>
                            <h4 class="title">Relationship Buildup</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Skill Section End ======-->




<?php
get_footer();