<?php
/*
    Template Name: Contact-Us Page
*/

get_header(); 

?>
    <!--====== Breadcumb Section start ======-->
    <section class="banner-section banner-section-three">
        <div class="banner-slider">
            <div class="single-banner">
                <div class="container-fluid container-1600">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <h2 class="wow fadeInLeft breadcumb-title">Contact Us</h2>
                            <ul class="breadcumb-list">
                                <li class="breadcumb-list_item"><a href="/">Home</a></li>
                                <li class="breadcumb-list_item">Contact Us</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="banner-shapes">
                    <div class="one"></div>
                    <div class="two"></div>
                    <div class="three"></div>
                </div>
                <div class="banner-line">
                    <img src="<?=get_template_directory_uri()?>/assets/img/lines/17.png" alt="Image">
                </div>
            </div>
        </div>
    </section>
    <!--====== Breadcumb Section end ======-->

    <!--====== Contact Section start ======-->
    <section class="contact-section contact-page section-gap">
        <div class="container">
            <div class="contact-info">
                <div class="row justify-content-center section-gap-bottom">
                    <div class="col-lg-6 order-2 order-lg-1">
                        <div class="illustration-img text-center">
                            <img src="<?=get_template_directory_uri()?>/assets/img/illustration/man-with-earth-02.png" alt="Image">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-10 order-1 order-lg-2">
                        <div class="contact-info-content">
                            <div class="section-title left-border mb-40">
                                <span class="title-tag">Get In Touch</span>
                                <h2 class="title">Need Any Help For Business & Consulting</h2>
                            </div>
                            <p>
                                But I must explain to you how all this mistaken idea of denouncing pleasure and praising
                                pain was
                            </p>

                            <ul>
                                <li class="phone">
                                    <a href="tel:+0123456789"><i class="far fa-phone"></i>+012 (345) 6789</a>
                                </li>
                                <li><i class="far fa-envelope-open"></i><a href="#">support@gmail.com</a></li>
                                <li><i class="far fa-map-marker-alt"></i>Broklyn Street USA</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="contact-form grey-bg">
                <div class="row no-gutters justify-content-center">
                    <div class="col-10">
                        <div class="section-title text-center mb-40">
                            <h2 class="title">Don’t Hesited To Contact Us</h2>
                        </div>

                        <form action="#">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="input-group mb-30">
                                        <input type="text" placeholder="Your Full Name">
                                        <span class="icon"><i class="far fa-user-circle"></i></span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="input-group mb-30">
                                        <input type="email" placeholder="Your Email Address">
                                        <span class="icon"><i class="far fa-envelope-open"></i></span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="input-group mb-30">
                                        <input type="text" placeholder="Your Phone">
                                        <span class="icon"><i class="far fa-phone"></i></span>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="input-group textarea mb-30">
                                        <textarea placeholder="Write Message"></textarea>
                                        <span class="icon"><i class="far fa-pencil"></i></span>
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <button type="submit" class="main-btn">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid container-1600">
            <div class="contact-map">
                <iframe
                     src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11986.587822625883!2d69.30057178996744!3d41.31654331061515!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x30050d13b577ae57!2zNDHCsDE4JzU4LjQiTiA2OcKwMTgnMDYuMSJF!5e0!3m2!1sru!2s!4v1641211514176!5m2!1sru!2s"
                    style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>
    </section>
    <!--====== Contact Section start ======-->

<?php
get_footer();