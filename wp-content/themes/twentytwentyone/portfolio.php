<?php
/*Template Name: Portfolio Page*/

get_header(); 

?>
    <!--====== Breadcumb Section start ======-->
    <section class="banner-section banner-section-three">
        <div class="banner-slider">
            <div class="single-banner">
                <div class="container-fluid container-1600">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <h2 class="wow fadeInLeft breadcumb-title">Portfolio</h2>
                            <ul class="breadcumb-list">
                                <li class="breadcumb-list_item"><a href="/">Home</a></li>
                                <li class="breadcumb-list_item">Portfolio</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="banner-shapes">
                    <div class="one"></div>
                    <div class="two"></div>
                    <div class="three"></div>
                </div>
                <div class="banner-line">
                    <img src="<?=get_template_directory_uri()?>/assets/img/lines/17.png" alt="Image">
                </div>
            </div>
        </div>
    </section>
    <!--====== Breadcumb Section end ======-->

    <!--====== Project section Start ======-->
    <section class="project-section">
        <div class="container">
            <div class="row align-items-center">
<!--                 <div class="col-lg-12 col-md-12">
                    <ul class="project-nav project-isotope-filter">
                        <li data-filter="*" class="active"> All Project </li>
                        <li data-filter=".item-one"> Business </li>
                        <li data-filter=".item-two"> Finaance </li>
                        <li data-filter=".item-three"> Development </li>
                        <li data-filter=".item-four"> Consulting </li>
                        <li data-filter=".item-five"> Web </li>
                        <li data-filter=".item-six"> Server </li>
                    </ul>
                </div> -->
            </div>

            <!-- Project Boxes -->
            <div class="row project-boxes project-isotope mt-60 justify-content-center">
                <div class="isotope-item col-lg-4 col-sm-6 item-one item-four">
                    <div class="project-box hover-style">
                        <div class="project-thumb">
                            <div class="thumb bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/project/05.jpg);"></div>
                        </div>
                        <div class="project-desc text-center">
                            <h4><a href="#">Free Consulting</a></h4>
                            <p>Join us for consultatoins</p>
                            <a href="#" class="project-link">
                                <i class="fal fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="isotope-item col-lg-4 col-sm-6 item-six item-three item-two">
                    <div class="project-box hover-style">
                        <div class="project-thumb">
                            <div class="thumb bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/project/03.jpg);"></div>
                        </div>
                        <div class="project-desc text-center">
                            <h4><a href="#">How To Business</a></h4>
                            <p>Join us for consultatoins</p>
                            <a href="#" class="project-link">
                                <i class="fal fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="isotope-item col-lg-4 col-sm-6 item-two item-five item-one">
                    <div class="project-box hover-style">
                        <div class="project-thumb">
                            <div class="thumb bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/project/14.jpg);"></div>
                        </div>
                        <div class="project-desc text-center">
                            <h4><a href="#">Design Strategy</a></h4>
                            <p>Join us for consultatoins</p>
                            <a href="#" class="project-link">
                                <i class="fal fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="isotope-item col-lg-4 col-sm-6 item-six item-one item-two">
                    <div class="project-box hover-style">
                        <div class="project-thumb">
                            <div class="thumb bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/project/02.jpg);"></div>
                        </div>
                        <div class="project-desc text-center">
                            <h4><a href="#">Research Strategy</a></h4>
                            <p>Join us for consultatoins</p>
                            <a href="#" class="project-link">
                                <i class="fal fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="isotope-item col-lg-4 col-sm-6 item-three item-five item-three">
                    <div class="project-box hover-style">
                        <div class="project-thumb">
                            <div class="thumb bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/project/10.jpg);"></div>
                        </div>
                        <div class="project-desc text-center">
                            <h4><a href="#">IT Consultations</a></h4>
                            <p>Join us for consultatoins</p>
                            <a href="#" class="project-link">
                                <i class="fal fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="isotope-item col-lg-4 col-sm-6 item-six item-four">
                    <div class="project-box hover-style">
                        <div class="project-thumb">
                            <div class="thumb bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/project/11.jpg);"></div>
                        </div>
                        <div class="project-desc text-center">
                            <h4><a href="#">Business Monitor</a></h4>
                            <p>Join us for consultatoins</p>
                            <a href="#" class="project-link">
                                <i class="fal fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="isotope-item col-lg-4 col-sm-6 item-four item-two item-five">
                    <div class="project-box hover-style">
                        <div class="project-thumb">
                            <div class="thumb bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/project/01.jpg);"></div>
                        </div>
                        <div class="project-desc text-center">
                            <h4><a href="#">Free Consulting</a></h4>
                            <p>Join us for consultatoins</p>
                            <a href="#" class="project-link">
                                <i class="fal fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="isotope-item col-lg-4 col-sm-6 item-one item-three">
                    <div class="project-box hover-style">
                        <div class="project-thumb">
                            <div class="thumb bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/project/12.jpg);">
                            </div>
                        </div>
                        <div class="project-desc text-center">
                            <h4><a href="#">Business Monitor</a></h4>
                            <p>Join us for consultatoins</p>
                            <a href="#" class="project-link">
                                <i class="fal fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="isotope-item col-lg-4 col-sm-6 item-five item-two">
                    <div class="project-box hover-style">
                        <div class="project-thumb">
                            <div class="thumb bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/project/13.jpg);">
                            </div>
                        </div>
                        <div class="project-desc text-center">
                            <h4><a href="#">IT Consultations</a></h4>
                            <p>Join us for consultatoins</p>
                            <a href="#" class="project-link">
                                <i class="fal fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Project section End ======-->

    <!--====== CTA Start ======-->
    <section class="cta-aection section-gap-bottom">
        <div class="container">
            <div class="cta-wrap bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/lines/16.png);">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="cta-content text-center">
                            <div class="section-title both-border mb-30">
                                <span class="title-tag">Get A Quote</span>
                                <h2 class="title">Feel Any Project For Business Consulting Get Started Us</h2>
                            </div>
                            <a href="#" class="main-btn main-btn-3">Get Started</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== CTA Start ======-->

<?php
get_footer();