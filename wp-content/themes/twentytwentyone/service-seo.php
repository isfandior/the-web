<?php
/*
Template Name: SEO service
Template Post Type: post, services 
*/

get_header();
require('template-parts/banner.php');
require 'template-parts/services_about.php';
?>
    <!--====== Fact Section Start ======-->
    <section class="fact-section-three pb-5">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-6 col-md-10 order-2 order-lg-1">
                    <div class="fact-text">
                        <div class="section-title left-border mb-40">
                            <span class="title-tag"><?=get_field('header_tag_seo_results')?></span>
                            <h2 class="title"><?=get_field('header_seo_results')?></h2>
                        </div>
                        <?=get_field('content_seo_results')?>
                    </div>
                </div>

                <div class="col-lg-6 col-md-10 order-1 order-lg-2">
                    <div class="fact-boxes row" id="factIsotpe">
                        <div class="col-6 col-tiny-12">
                            <div class="fact-box fact-box-three text-center">
                                <div class="icon">
                                    <i class="flaticon-graphic"></i>
                                </div>
                                <h2 class="counter"><?=get_field('seo_result_number_1')?></h2>
                                <p class="title"><?=get_field('seo_result_title_1')?></p>
                            </div>
                        </div>
                        <div class="col-6 col-tiny-12">
                            <div class="fact-box fact-box-three text-center mt-30">
                                <div class="icon">
                                    <i class="flaticon-plan"></i>
                                </div>
                                <h2 class="counter"><?=get_field('seo_result_number_2')?></h2>
                                <p class="title"><?=get_field('seo_result_title_2')?></p>
                            </div>
                        </div>
                        <div class="col-6 col-tiny-12">
                            <div class="fact-box fact-box-three text-center mt-30">
                                <div class="icon">
                                    <i class="flaticon-target-1"></i>
                                </div>
                                <h2 class="counter"><?=get_field('seo_result_number_3')?></h2>
                                <p class="title"><?=get_field('seo_result_title_3')?></p>
                            </div>
                        </div>
                        <div class="col-6 col-tiny-12">
                            <div class="fact-box fact-box-three text-center mt-30">
                                <div class="icon">
                                    <i class="flaticon-teamwork"></i>
                                </div>
                                <h2 class="counter"><?=get_field('seo_result_number_4')?></h2>
                                <p class="title"><?=get_field('seo_result_title_4')?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Fact Section End ======-->



<?php
require 'template-parts/service_price.php';
require 'template-parts/cta.php';
get_footer();