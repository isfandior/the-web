<?php  get_header();
?>

<!--====== Breadcrumb part Start ======-->
<section class="banner-section banner-section-three">
    <div class="banner-slider">
        <div class="single-banner">
            <div class="container-fluid container-1600">
                <div class="row align-items-center">
                    <div class="col-md-5">
                        <div class="banner-content">
                            <span class="promo-text wow fadeInLeft" data-wow-duration="1500ms"
                                  data-wow-delay="400ms"><?=get_field('banner_subtitle')?></span>
                            <h1 class="wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="500ms">
                                <?=get_the_title()?>
                            </h1>
                            <p></p>
                            <ul class="btn-wrap">
                                <li class="wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="600ms">
                                    <a href="#modal" data-toggle="modal" data-target="#modal" class="main-btn"><?=get_btn_text_by_lang('start_project')?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-7 d-none d-md-block">
                        <div class="banner-img text-right wow fadeInRight" data-wow-duration="1500ms"
                             data-wow-delay="800ms">
                            <img src="<?=get_field('banner_img')?>" alt="illustration">
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-shapes">
                <div class="one"></div>
                <div class="two"></div>
                <div class="three"></div>
            </div>
            <div class="banner-line">
                <img src="<?php echo get_template_directory_uri (); ?>/assets/img/lines/17.png" alt="Image">
            </div>
        </div>
    </div>
</section>
<!--====== Breadcrumb part End ======-->

<!--====== Project Details Start ======-->
<section class="project-details section-gap">
    <div class="container">
        <div class="project-content mt-60">
            <div class="row">
                <div class="col-lg-8 order-2 order-lg-2">
                    <div class="content">
                        <div class="mrb-40">
                            <h2><?=get_field('project_header')?></h2>
                            <img src="<?= get_field('portfolio_photo')?>" alt="">
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 order-1 order-lg-2">
                    <div class="details">
                        <ul>
                            <li>
                                <h3>Компания:</h3>
                                <p><?=get_field('project_info_text_1')?></p>
                            </li>
                            <li>
                                <h3>Услуги:</h3>
                                <p><?=get_field('project_info_text_2')?></p>
                            </li>
                            <li>
                                <h3>Ссылка на сайт:</h3>
                                <p><a href="<?=get_field('url')?>" target="_blank"><?=get_field('project_header',)?></a></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--====== Project Details End ======-->




<?php
require('template-parts/cta.php');
?>

<?php get_footer();
?>
