<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();
?>


    <section class="about-section about-illustration-img section-gap" style="margin-top: 100px">
        <div class="container">
            <div class="illustration-img">
                <img src="<?=get_template_directory_uri()?>/assets/img/404.png" alt="Image">
            </div>
            <div class="row no-gutters justify-content-lg-end justify-content-center">
                <div class="col-lg-6 col-md-10">
                    <div class="about-text">
                        <div class="section-title left-border mb-40">
                            <span class="title-tag">Ошибка!</span>
                            <h2 class="title">Похоже такой страницы нет</h2>
                        </div>
                        <p class="mb-25">
                            Предлагаем вернуться на главную страницу
                        </p>
                        <a href="/" class="main-btn">Главная страница</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
