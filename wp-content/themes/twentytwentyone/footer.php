<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */


$lang = pll_current_language();
if ($lang == 'ru') {
    $menu = 'Навигация';
    $blog = 'Статьи';
    $contact = 'Связаться с нами';
    $adress = get_field('adress_ru',29);
}else{
    $menu = 'Menu';
    $blog = 'Maqolalar';
    $contact = 'Aloqa';
    $adress = get_field('adress_uz',29);
}

 ?>
<!--====== Footer Section Start ======-->
    <footer class="footer">
        <div class="container">
            <div class="footer-widget">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 order-1">
                        <div class="widget site-info-widget">
                            <div class="footer-logo">
                                <img class="f-logo" style="width: 200px;" src="<?=get_template_directory_uri()?>/assets/img/logo-2.png" alt="The Web">
                            </div>
                            <p><?=get_field('footer_text')?></p>
                            <!--<ul class="social-links">
                                <li><a href="<?php /*=get_field('social_facebook',29)*/?>"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="<?php /*=get_field('social_instagram',29)*/?>"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="<?php /*=get_field('social_telegram',29)*/?>"><i class="fab fa-telegram"></i></a></li>
                                <li><a href="<?php /*=get_field('social_linkedin',29)*/?>"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>-->
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 order-3">
                        <div class="widget nav-widget">
                            <h4 class="widget-title"><?=$menu?></h4>
                            <?php 
                                $menu_args = [
                                    'items_wrap' => '<ul id="%1$s" class="%2$s nav-menu">%3$s</ul>',
                                    'container' => false
                                ];
                                wp_nav_menu($menu_args);
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-5 order-lg-4 order-5">
                        <div class="row">
                            <?php if ($blog_art != false): ?>
                            <div class="col-lg-6 col-sm-6">
                                <div class="widget nav-widget">
                                    <h4 class="widget-title"><?=$blog?></h4>
                                    <ul>
                                        <li><a href="#">About Comapny</a></li>
                                        <li><a href="#">World Wide Clients</a></li>
                                        <li><a href="#">Happy People’s</a></li>
                                        <li><a href="#">Winning Awards</a></li>
                                        <li><a href="#">Company Statics</a></li>
                                    </ul>
                                </div>
                            </div>
                            <?php endif ?>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="widget contact-widget">
                                    <h4 class="widget-title"><?=$contact?></h4>
                                    <p><?=get_btn_text_by_lang('phone-us')?></p>
                                    <ul class="contact-infos">
                                        <li>
                                            <a href="tel:<?=get_field('phone_1',29)?>">
                                                <i class="far fa-phone"></i>
                                                <?=get_field('phone_1',29)?>
                                            </a>
                                        </li>
                                        <!--<li>
                                            <a href="tel:<?php /*=get_field('phone_2',29)*/?>">
                                                <i class="far fa-phone"></i>
                                                <?php /*=get_field('phone_2',29)*/?>
                                            </a>
                                        </li>-->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p class="copyright-text">
                   <a href="/">THEWEB</a>  <?=date('Y')?></span>
                </p>
                <a href="#" class="back-to-top"><i class="far fa-angle-up"></i></a>
            </div>
        </div>


        <img src="<?=get_template_directory_uri()?>/assets/img/lines/01.png" alt="line-shape" class="line-one">
        <img src="<?=get_template_directory_uri()?>/assets/img/lines/02.png" alt="line-shape" class="line-two">

        <img src="<?=get_template_directory_uri()?>/assets/img/lines/09.png" alt="line-shape" class="line-three">
        <img src="<?=get_template_directory_uri()?>/assets/img/lines/10.png" alt="line-shape" class="line-four">
    </footer>
    <!--====== Footer Section end ======-->
    
    <script src="<?=get_template_directory_uri()?>/assets/js/vendor/jquery-1.12.4.min.js"></script>
    
    <!--====== Bootstrap js ======-->
    <script src="<?=get_template_directory_uri()?>/assets/js/bootstrap.min.js"></script>
    <script src="<?=get_template_directory_uri()?>/assets/js/popper.min.js"></script>
    <!--====== Mask js ======-->
    <script src="https://unpkg.com/imask"></script>
    <!--====== Slick js ======-->
    <script src="<?=get_template_directory_uri()?>/assets/js/slick.min.js"></script>
    <!--====== Isotope js ======-->
    <script src="<?=get_template_directory_uri()?>/assets/js/isotope.pkgd.min.js"></script>
    <!--====== Magnific Popup js ======-->
    <script src="<?=get_template_directory_uri()?>/assets/js/jquery.magnific-popup.min.js"></script>
    <!--====== inview js ======-->
    <script src="<?=get_template_directory_uri()?>/assets/js/jquery.inview.min.js"></script>
    <!--====== counterup js ======-->
    <script src="<?=get_template_directory_uri()?>/assets/js/jquery.countTo.js"></script>
    <!--====== easy PieChart js ======-->
    <script src="<?=get_template_directory_uri()?>/assets/js/jquery.easypiechart.min.js"></script>
    <!--====== Jquery Ui ======-->
    <script src="<?=get_template_directory_uri()?>/assets/js/jquery-ui.min.js"></script>
    <!--====== Wow JS ======-->
    <script src="<?=get_template_directory_uri()?>/assets/js/wow.min.js"></script>
    <!--====== Main js ======-->
    <script src="<?=get_template_directory_uri()?>/assets/js/main.js"></script>
    <script src="<?=get_template_directory_uri()?>/assets/js/radio-slider.min.js"></script>
    <script src="<?=get_template_directory_uri()?>/assets/js/custom.js"></script>
    <?php require('template-parts/modal.php') ?>
    <script>
        function goToUrl(url) {
            window.open(url, '_blank');
        }
    </script>
</body>

</html>


