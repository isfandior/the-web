<?php
/*
Template Name: Adword service
Template Post Type: post, services 
*/

get_header(); 

?>
<?php
require 'template-parts/banner.php';
?>
<section class="section-gap-bottom"></section>
<?php
require 'template-parts/service_price.php';
require 'template-parts/faq.php';
require 'template-parts/cta.php';
?>




<?php
get_footer();