<?php
/*
    Template Name: Thank you Page
*/
$lang = pll_current_language();
/*if (empty($_POST)) {
    header('location: /');
    exit;
}*/
if ($lang == 'ru') {
    $title = 'Благодарим за обращение!';
    $subtitle = 'Скоро мы с Вами свяжемся.';
}
else{
    $title = 'Murojaat uchun rahmat!';
    $subtitle = 'Tez orada siz bilan aloqaga chiqamiz.';
}
$send = $_POST;
send_to_telegram($send);
get_header(); 
?>
<section class="banner-section banner-section-three">
    <div class="banner-slider">
        <div class="single-banner">
            <div class="container-fluid container-1600">
                <div class="row align-items-center">
                    <div class="col-md-5">
                        <div class="banner-content">
                            <span class="promo-text wow fadeInLeft" data-wow-duration="1500ms"
                            data-wow-delay="400ms"><?=get_field('banner_subtitle')?></span>
                            <h1 class="wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="500ms">
                                <?=$title?>
                            </h1>
                            <p><?=$subtitle?></p>
                            <ul class="btn-wrap">
                                <li class="wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="700ms">
                                    <a href="#service-section" class="main-btn main-btn-3"><?=get_btn_text_by_lang('our_service')?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-7 d-none d-md-block">
                        <div class="banner-img text-right wow fadeInRight" data-wow-duration="1500ms"
                        data-wow-delay="800ms">
                            <img class="thank-you-banner-img" src="<?=get_template_directory_uri()?>/assets/img/thank-you.svg" alt="illustration">
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-shapes">
                <div class="one"></div>
                <div class="two"></div>
                <div class="three"></div>
            </div>
            <div class="banner-line">
                <img src="<?=get_template_directory_uri()?>/assets/img/lines/17.png" alt="Image">
            </div>
        </div>
    </div>
</section>
<?php
get_footer();

?>