<?php
/*Template Name: Blog Detail Page*/

get_header(); 

?>
    <!--====== Breadcumb Section start ======-->
    <section class="banner-section banner-section-three">
        <div class="banner-slider">
            <div class="single-banner">
                <div class="container-fluid container-1600">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <h2 class="wow fadeInLeft breadcumb-title">Blog Details</h2>
                            <ul class="breadcumb-list">
                                <li class="breadcumb-list_item"><a href="/">Home</a></li>
                                <li class="breadcumb-list_item">Blog Details</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="banner-shapes">
                    <div class="one"></div>
                    <div class="two"></div>
                    <div class="three"></div>
                </div>
                <div class="banner-line">
                    <img src="<?=get_template_directory_uri()?>/<?=get_template_directory_uri()?>/<?=get_template_directory_uri()?>/assets/img/lines/17.png" alt="Image">
                </div>
            </div>
        </div>
    </section>
    <!--====== Breadcumb Section end ======-->

    <!--====== Blog Section Start ======-->
    <section class="blog-section section-gap-top">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 section-gap-bottom">
                    <!-- Blog Details -->
                    <div class="post-details-wrap">
                        <div class="post-thumb">
                            <img src="<?=get_template_directory_uri()?>/assets/img/blog/12.jpg" alt="Image">
                        </div>
                        <div class="post-meta">
                            <ul>
                                <li><i class="far fa-user"></i><a href="#">Nichel Jhon</a></li>
                                <li><i class="far fa-calendar-alt"></i><a href="#">25 Aug 2020</a></li>
                                <li><i class="far fa-comments"></i><a href="#">Comments (05)</a></li>
                            </ul>
                        </div>
                        <div class="post-content">
                            <h3 class="title">
                                Inspired Design Decisions With Herb Typography Can Be As Exciting As
                                Illustration & Photo
                            </h3>
                            <p>
                                But I must explain to you how all this mistaken idea of denouncing pleasure and praising
                                pain was born and I will give you a complete account of the system, and expound the
                                actual teachings of the great explorer of the truth, the master-builder of human
                                happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure,
                                but because those who do not know how to pursue pleasure rationally encounter
                                consequences that are extremely painful. Nor again is there anyone who loves or pursues
                                or desires to obtain pain of itself, because it is pain, but because occasionally
                                circumstances occur in which toil and pain can procure him some great pleasure. To take
                                a trivial example, which of us ever undertakes laborious physical exercise, except to
                                obtain some advantage from it but who has any right to find fault with a man who chooses
                            </p>

                            <blockquote>
                                Smashing Podcast Episode With Paul Boag What Is Conversion Optimization
                                <span class="author">Bailey Dobson</span>
                            </blockquote>

                            <h4 class="with-check"><i class="far fa-check-circle"></i> Inspired Design Decisions With
                                Otto Storch When Idea Copy</h4>

                            <p>
                                No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because
                                those who do not know how to pursue pleasure rationally encounter consequences that are
                                extremely painful. Nor again is there anyone who loves or pursues or desires to obtain
                                pain of itself, because it is pain, but because occasionally circumstances occur in
                                which toil and pain can procure him some great pleasure.
                            </p>

                        </div>
                    </div>
                    <!-- Comments Template -->
                    <div class="comment-template">
                        <h3 class="title section-gap-top">Comments</h3>
                        <!-- Comments form -->
                        <div class="comment-form">
                            <h3 class="title">Leave A Reply</h3>
                            <form action="#">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="input-group mb-30">
                                            <input type="text" placeholder="Your Full Name">
                                            <span class="icon"><i class="far fa-user"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="input-group mb-30">
                                            <input type="email" placeholder="Your Email ">
                                            <span class="icon"><i class="far fa-envelope"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="input-group textarea mb-30">
                                            <textarea placeholder="Write Message"></textarea>
                                            <span class="icon"><i class="far fa-pencil"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="main-btn">Send Reply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-8">
                    <!-- sidebar -->
                    <div class="sidebar">
                        <!-- Recent Post Widget -->
                        <div class="widget recent-post-widget">
                            <h4 class="widget-title">Recent News</h4>

                            <div class="post-loops">
                                <div class="single-post">
                                    <div class="post-thumb">
                                        <img src="<?=get_template_directory_uri()?>/assets/img/sidebar/recent-01.jpg" alt="Image">
                                    </div>
                                    <div class="post-desc">
                                        <span class="date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                        <a href="#">
                                            Smashing Podcast Epis With Rach Andrewe
                                        </a>
                                    </div>
                                </div>
                                <div class="single-post">
                                    <div class="post-thumb">
                                        <img src="<?=get_template_directory_uri()?>/assets/img/sidebar/recent-02.jpg" alt="Image">
                                    </div>
                                    <div class="post-desc">
                                        <span class="date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                        <a href="#">
                                            Responsive Web And Desktop Develop
                                        </a>
                                    </div>
                                </div>
                                <div class="single-post">
                                    <div class="post-thumb">
                                        <img src="<?=get_template_directory_uri()?>/assets/img/sidebar/recent-03.jpg" alt="Image">
                                    </div>
                                    <div class="post-desc">
                                        <span class="date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                        <a href="#">
                                            Django Highlig Models Admin Harnessing
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- CTA Widget -->
                        <div class="widget cta-widget" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/sidebar/cta.jpg);">
                            <h4 class="title">Need Any Consultations</h4>
                            <a href="#" class="main-btn">Send Message</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Blog Section End ======-->


<?php
get_footer();