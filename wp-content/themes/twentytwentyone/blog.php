<?php
/*Template Name: Blog Page*/

get_header(); 

?>
    <!--====== Banner Section start ======-->
    <section class="banner-section banner-section-three">
        <div class="banner-slider">
            <div class="single-banner">
                <div class="container-fluid container-1600">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <h2 class="wow fadeInLeft breadcumb-title">Blog Details</h2>
                            <ul class="breadcumb-list">
                                <li class="breadcumb-list_item"><a href="/">Home</a></li>
                                <li class="breadcumb-list_item">Blog Details</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="banner-shapes">
                    <div class="one"></div>
                    <div class="two"></div>
                    <div class="three"></div>
                </div>
                <div class="banner-line">
                    <img src="<?=get_template_directory_uri()?>/assets/img/lines/17.png" alt="Image">
                </div>
            </div>
        </div>
    </section>
    <!--====== Banner Section end ======-->


    <!--====== Blog Section Start ======-->
    <section class="blog-section section-gap-top">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <!-- Blog loop(Grid) -->
                    <div class="blog-loop grid-blog row justify-content-center">
                        <!-- Single Post -->
                        <div class="col-lg-6 col-md-6 col-10 col-tiny-12">
                            <div class="single-post-box">
                                <div class="post-thumb">
                                    <img src="<?=get_template_directory_uri()?>/assets/img/blog/04.jpg" alt="Image">
                                </div>
                                <div class="post-content">
                                    <span class="post-date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                    <h3 class="title">
                                        <a href="#">
                                            How Performance Visiblety With GitLab CI And Hood
                                        </a>
                                    </h3>
                                    <p>
                                        Sedut perspiciatis unde omnis nat erroroluptat accusantium laudantim totam
                                        raperiaeaqupsa quae
                                    </p>
                                    <a href="#" class="post-link">
                                        Learn More <i class="far fa-long-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Single Post -->
                        <div class="col-lg-6 col-md-6 col-10 col-tiny-12">
                            <div class="single-post-box">
                                <div class="post-thumb">
                                    <img src="<?=get_template_directory_uri()?>/assets/img/blog/05.jpg" alt="Image">
                                </div>
                                <div class="post-content">
                                    <span class="post-date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                    <h3 class="title">
                                        <a href="#">
                                            Inspired Design Decisions With Max Huber Turne
                                        </a>
                                    </h3>
                                    <p>
                                        Sedut perspiciatis unde omnis nat erroroluptat accusantium laudantim totam
                                        raperiaeaqupsa quae
                                    </p>
                                    <a href="#" class="post-link">
                                        Learn More <i class="far fa-long-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Single Post -->
                        <div class="col-lg-6 col-md-6 col-10 col-tiny-12">
                            <div class="single-post-box">
                                <div class="post-thumb">
                                    <img src="<?=get_template_directory_uri()?>/assets/img/blog/06.jpg" alt="Image">
                                </div>
                                <div class="post-content">
                                    <span class="post-date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                    <h3 class="title">
                                        <a href="blog-details.html">
                                            Mirage JS Deep Dive Unders Tanding Mirage JS Models
                                        </a>
                                    </h3>
                                    <p>
                                        Sedut perspiciatis unde omnis nat erroroluptat accusantium laudantim totam
                                        raperiaeaqupsa quae
                                    </p>
                                    <a href="blog-details.html" class="post-link">
                                        Learn More <i class="far fa-long-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Single Post -->
                        <div class="col-lg-6 col-md-6 col-10 col-tiny-12">
                            <div class="single-post-box">
                                <div class="post-thumb">
                                    <img src="<?=get_template_directory_uri()?>/assets/img/blog/07.jpg" alt="Image">
                                </div>
                                <div class="post-content">
                                    <span class="post-date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                    <h3 class="title">
                                        <a href="blog-details.html">
                                            Brighten Up Someone’s May With Max Huber Turne
                                        </a>
                                    </h3>
                                    <p>
                                        Sedut perspiciatis unde omnis nat erroroluptat accusantium laudantim totam
                                        raperiaeaqupsa quae
                                    </p>
                                    <a href="blog-details.html" class="post-link">
                                        Learn More <i class="far fa-long-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Single Post -->
                        <div class="col-lg-6 col-md-6 col-10 col-tiny-12">
                            <div class="single-post-box">
                                <div class="post-thumb">
                                    <img src="<?=get_template_directory_uri()?>/assets/img/blog/08.jpg" alt="Image">
                                </div>
                                <div class="post-content">
                                    <span class="post-date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                    <h3 class="title">
                                        <a href="blog-details.html">
                                            Why Collaborative Coding Is The Ultimate Career Hack
                                        </a>
                                    </h3>
                                    <p>
                                        Sedut perspiciatis unde omnis nat erroroluptat accusantium laudantim totam
                                        raperiaeaqupsa quae
                                    </p>
                                    <a href="blog-details.html" class="post-link">
                                        Learn More <i class="far fa-long-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Single Post -->
                        <div class="col-lg-6 col-md-6 col-10 col-tiny-12">
                            <div class="single-post-box">
                                <div class="post-thumb">
                                    <img src="<?=get_template_directory_uri()?>/assets/img/blog/09.jpg" alt="Image">
                                </div>
                                <div class="post-content">
                                    <span class="post-date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                    <h3 class="title">
                                        <a href="blog-details.html">
                                            Responsive Web & Desktop Development Flutter
                                        </a>
                                    </h3>
                                    <p>
                                        Sedut perspiciatis unde omnis nat erroroluptat accusantium laudantim totam
                                        raperiaeaqupsa quae
                                    </p>
                                    <a href="blog-details.html" class="post-link">
                                        Learn More <i class="far fa-long-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Single Post -->
                        <div class="col-lg-6 col-md-6 col-10 col-tiny-12">
                            <div class="single-post-box">
                                <div class="post-thumb">
                                    <img src="<?=get_template_directory_uri()?>/assets/img/blog/10.jpg" alt="Image">
                                </div>
                                <div class="post-content">
                                    <span class="post-date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                    <h3 class="title">
                                        <a href="blog-details.html">
                                            How To Create Particle Trail Animation In JavaScript
                                        </a>
                                    </h3>
                                    <p>
                                        Sedut perspiciatis unde omnis nat erroroluptat accusantium laudantim totam
                                        raperiaeaqupsa quae
                                    </p>
                                    <a href="blog-details.html" class="post-link">
                                        Learn More <i class="far fa-long-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Single Post -->
                        <div class="col-lg-6 col-md-6 col-10 col-tiny-12">
                            <div class="single-post-box">
                                <div class="post-thumb">
                                    <img src="<?=get_template_directory_uri()?>/assets/img/blog/11.jpg" alt="Image">
                                </div>
                                <div class="post-content">
                                    <span class="post-date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                    <h3 class="title">
                                        <a href="blog-details.html">
                                            Inspired Design Decisions With Herb Lubalin Can
                                        </a>
                                    </h3>
                                    <p>
                                        Sedut perspiciatis unde omnis nat erroroluptat accusantium laudantim totam
                                        raperiaeaqupsa quae
                                    </p>
                                    <a href="blog-details.html" class="post-link">
                                        Learn More <i class="far fa-long-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Pagination -->
                    <div class="pagination-wrap">
                        <ul>
                            <li><a href="#"><i class="far fa-angle-left"></i></a></li>
                            <li class="active"><a href="#">01</a></li>
                            <li><a href="#">02</a></li>
                            <li><a href="#">03</a></li>
                            <li><a href="#"><i class="far fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-8">
                    <!-- sidebar -->
                    <div class="sidebar">
                        <!-- Search Widget -->
                        <div class="widget search-widget">
                            <form action="#">
                                <input type="text" placeholder="Search here">
                                <button type="submit"><i class="far fa-search"></i></button>
                            </form>
                        </div>
                        <!-- Cat Widget -->
                        <div class="widget cat-widget">
                            <h4 class="widget-title">Category</h4>

                            <ul>
                                <li>
                                    <a href="#">Financial Planning <span><i class="far fa-angle-right"></i></span></a>
                                </li>
                                <li>
                                    <a href="#">Relationship Buildup <span><i class="far fa-angle-right"></i></span></a>
                                </li>
                                <li>
                                    <a href="#">Investement Planning <span><i class="far fa-angle-right"></i></span></a>
                                </li>
                                <li>
                                    <a href="#">Marketing Strategy <span><i class="far fa-angle-right"></i></span></a>
                                </li>
                                <li>
                                    <a href="#">Product Strategy <span><i class="far fa-angle-right"></i></span></a>
                                </li>
                            </ul>
                        </div>
                        <!-- Recent Post Widget -->
                        <div class="widget recent-post-widget">
                            <h4 class="widget-title">Recent News</h4>

                            <div class="post-loops">
                                <div class="single-post">
                                    <div class="post-thumb">
                                        <img src="<?=get_template_directory_uri()?>/assets/img/sidebar/recent-01.jpg" alt="Image">
                                    </div>
                                    <div class="post-desc">
                                        <span class="date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                        <a href="#">
                                            Smashing Podcast Epis With Rach Andrewe
                                        </a>
                                    </div>
                                </div>
                                <div class="single-post">
                                    <div class="post-thumb">
                                        <img src="<?=get_template_directory_uri()?>/assets/img/sidebar/recent-02.jpg" alt="Image">
                                    </div>
                                    <div class="post-desc">
                                        <span class="date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                        <a href="#">
                                            Responsive Web And Desktop Develop
                                        </a>
                                    </div>
                                </div>
                                <div class="single-post">
                                    <div class="post-thumb">
                                        <img src="<?=get_template_directory_uri()?>/assets/img/sidebar/recent-03.jpg" alt="Image">
                                    </div>
                                    <div class="post-desc">
                                        <span class="date"><i class="far fa-calendar-alt"></i>25 Aug 2020</span>
                                        <a href="#">
                                            Django Highlig Models Admin Harnessing
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Popular Tag Widget -->
                        <div class="widget popular-tag-widget">
                            <h4 class="widget-title">Popular Tags</h4>
                            <div class="tags-loop">
                                <a href="#">Business</a>
                                <a href="#">Corporate</a>
                                <a href="#">HTML</a>
                                <a href="#">Finance</a>
                                <a href="#">Investment</a>
                                <a href="#">CSS</a>
                                <a href="#">Planing</a>
                                <a href="#">Creative</a>
                            </div>
                        </div>
                        <!-- Author Infor Widget -->
                        <div class="widget author-widget">
                            <img src="<?=get_template_directory_uri()?>/assets/img/sidebar/author.jpg" alt="Image" class="author-img">
                            <h4 class="name">Brandon Johnston</h4>
                            <span class="role">Author</span>
                            <p>
                                Vero eos et accusamus et iustoys odio dignissimos ducimu blanditiis praesentium
                                voluptatum
                            </p>
                            <ul class="social-icons">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fab fa-behance"></i></a></li>
                                <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                        <!-- CTA Widget -->
                        <div class="widget cta-widget" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/sidebar/cta.jpg);">
                            <h4 class="title">Need Any Consultations</h4>
                            <a href="#" class="main-btn">Send Message</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Blog Section End ======-->

    <section class="section-gap"></section>

    <!--====== Service Section Start ======-->
    <section class="service-section grey-bg service-line-shape section-gap">
        <div class="container">
            <!-- Section Title -->
            <div class="section-title text-center both-border mb-50">
                <span class="title-tag">Most Features</span>
                <h2 class="title">We Provide Most Exclusive <br> Service For Business</h2>
            </div>
            <!-- Services Boxes -->
            <div class="row service-boxes justify-content-center">
                <div class="col-lg-3 col-sm-6 col-10 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="800ms">
                    <div class="service-box-three border-0">
                        <div class="icon">
                            <img src="<?=get_template_directory_uri()?>/assets/img/icons/01.png" alt="Icon">
                        </div>
                        <h3><a href="#">Creative Idea</a></h3>
                        <p>Sed perspicia unde omnis</p>
                        <a href="#" class="service-link">
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-10 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="900ms">
                    <div class="service-box-three border-0">
                        <div class="icon">
                            <img src="<?=get_template_directory_uri()?>/assets/img/icons/02.png" alt="Icon">
                        </div>
                        <h3><a href="#">Business Strategy</a></h3>
                        <p>Quis autem velrepres hend</p>
                        <a href="#" class="service-link">
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-10 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="1s">
                    <div class="service-box-three border-0">
                        <div class="icon">
                            <img src="<?=get_template_directory_uri()?>/assets/img/icons/03.png" alt="Icon">
                        </div>
                        <h3><a href="#">Relationship</a></h3>
                        <p>Sed perspicia unde omnis</p>
                        <a href="#" class="service-link">
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-10 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="1.1s">
                    <div class="service-box-three border-0">
                        <div class="icon">
                            <img src="<?=get_template_directory_uri()?>/assets/img/icons/04.png" alt="Icon">
                        </div>
                        <h3><a href="#">Productivity</a></h3>
                        <p>Quis autem velrepres hend</p>
                        <a href="#" class="service-link">
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="line-one">
            <img src="<?=get_template_directory_uri()?>/assets/img/lines/12.png" alt="line-shape">
        </div>
        <div class="line-two">
            <img src="<?=get_template_directory_uri()?>/assets/img/lines/11.png" alt="line-shape">
        </div>
        <div class="working-circle"></div>
    </section>
    <!--====== Service Section End ======-->

    <section class="section-gap"></section>



<?php
get_footer();