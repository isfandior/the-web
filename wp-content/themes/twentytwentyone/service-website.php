<?php
/*
Template Name: Website service
Template Post Type: post, services 
*/
get_header();
?>
<?php
    require 'template-parts/banner.php';
    require 'template-parts/services_about.php';
    require 'template-parts/service_features.php';
    require 'template-parts/calc.php';
    require 'template-parts/service_why_us.php';
    require 'template-parts/how_we_work.php';
    require 'template-parts/service_price.php';
    require 'template-parts/faq.php';
    require 'template-parts/portfolio.php';
    require 'template-parts/cta.php';
    ?>
<?php
get_footer();