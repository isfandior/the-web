<?php
/*
    Template Name: About-Us Page
*/

get_header(); 

?>
<?php 
$lang = pll_current_language();
if ($lang == 'ru') {
    $home_url = 'Главная';
}else{
    $home_url = 'Bosh Sahifa';
}

?>
    <!--====== Breadcumb Section start ======-->
    <section class="banner-section banner-section-three">
        <div class="banner-slider">
            <div class="single-banner">
                <div class="container-fluid container-1600">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <h2 class="wow fadeInLeft breadcumb-title"><?php single_post_title(); ?></h2>
                            <ul class="breadcumb-list">
                                <li class="breadcumb-list_item"><a href="/"><?=$home_url?></a></li>
                                <li class="breadcumb-list_item"><?php single_post_title(); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="banner-shapes">
                    <div class="one"></div>
                    <div class="two"></div>
                    <div class="three"></div>
                </div>
                <div class="banner-line">
                    <img src="<?=get_template_directory_uri()?>/assets/img/lines/17.png" alt="Image">
                </div>
            </div>
        </div>
    </section>
    <!--====== Breadcumb Section end ======-->

    <!--====== About Section start ======-->
    <section class="about-section about-illustration-img section-gap">
        <div class="container">
            <div class="illustration-img">
                <img src="<?=get_field('about_img')?>" alt="Image">
            </div>
            <div class="row no-gutters justify-content-lg-end justify-content-center">
                <div class="col-lg-6 col-md-10">
                    <div class="about-text">
                        <div class="section-title left-border mb-40">
                            <span class="title-tag"><?=get_field('about_section_subtitle')?></span>
                            <h2 class="title"><?=get_field('about_section_title')?></h2>
                        </div>
                        <?=get_field('about_desc')?>
                        <a href="<?=get_field('button_url')?>" class="main-btn"><?=get_btn_text_by_lang('more')?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== About Section end ======-->

    <?php require ('template-parts/services_bg.php') ?>

    <?php require('template-parts/cta.php'); ?>

<?php
get_footer();