<?php 
disable_uz();
/**
 * The header.
 *
 * This is the template that displays all of the <head> section and everything up until main.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!--====== Title ======-->
    <title><?=get_the_title()?></title>
    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="<?=get_template_directory_uri()?>/assets/img/favicon.ico" type="img/png" />
    <!--====== Animate Css ======-->
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/css/animate.min.css">
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/css/bootstrap.min.css" />
    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/css/font-awesome.min.css" />
    <!--====== Flaticon css ======-->
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/css/flaticon.css" />
    <!--====== Magnific Popup css ======-->
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/css/magnific-popup.css" />
    <!--====== Slick  css ======-->
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/css/slick.css" />
    <!--====== Jquery ui ======-->
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/css/jquery-ui.min.css" />
    <!--====== Style css ======-->
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/css/radio-slider.min.css" />
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/css/style.css" />
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/css/stylej.css" />
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-11333306932"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-11333306932');
    </script>
    <!-- Event snippet for Отправка формы для потенциальных клиентов conversion page -->
    <script>
        gtag('event', 'conversion', {'send_to': 'AW-11333306932/O7EMCM3_zeAYELSUkpwq'});
    </script>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-RN9DQ8SK26"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-RN9DQ8SK26');
    </script>
    <?php wp_head(); ?>

</head>
<body>
    <?php wp_body_open(); ?>
    <!--====== Preloader ======-->
    <!--div id="preloader">
        <div class="loader-cubes">
            <div class="loader-cube1 loader-cube"></div>
            <div class="loader-cube2 loader-cube"></div>
            <div class="loader-cube4 loader-cube"></div>
            <div class="loader-cube3 loader-cube"></div>
        </div>
    </div>-->
    <header class="header-three sticky-header">
        <div class="header-topbar">
            <div class="container-fluid container-1600">
                <div class="header-topbar-inner d-md-flex align-items-center justify-content-between">
                    <!-- Contact Info -->
                    <ul class="contact-info">
                        <li>
                            <a href="tel:<?=get_field('phone_1',29)?>"><i class="far fa-phone"></i><?=get_field('phone_1',29)?></a>
                        </li>
                        <!--<li>
                            <a href="tel:<?php /*=get_field('phone_1',29)*/?>"><i class="far fa-phone"></i><?php /*=get_field('phone_2',29)*/?></a>
                        </li>-->
                    </ul>
                    <!-- Social Links -->
                    <!-- <ul class="social-links">
                        <li>
                            <i class="fas fa-globe-americas lang-icon"></i>
                            <a class="lang-text" href="/">Русский</a>
                        </li>
                        <li>
                            <a class="lang-text" href="/uz">O'zbekcha</a>
                        </li>
                    </ul> -->
                </div>
            </div>
        </div>
        <div class="header-nav">
            <div class="container-fluid">
                <div class="nav-container">
                    <!-- Site Logo -->
                    <div class="site-logo">
                        <a href="/"><img class="logo" src="<?=get_template_directory_uri()?>/assets/img/logo.png" width="200" alt="Logo"></a>
                    </div>

                    <!-- Main Menu -->
                    <div class="nav-menu d-lg-flex align-items-center">

                        <!-- Navbar Close Icon -->
                        <div class="navbar-close">
                            <div class="cross-wrap">
                                <span></span>
                                <span></span>
                            </div>
                        </div>

                        <!-- Mneu Items -->
                        <div class="menu-items">
                            
                            <?php 
                                $menu_args = [
                                    'items_wrap' => '<ul id="%1$s" class="%2$s ">%3$s</ul>',
                                    'container' => false,
                                ];
                                wp_nav_menu($menu_args);
                            ?>
                            <!-- Contact Info -->
                            <ul class="nav-contact">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page">
                                    <a href="tel:<?=get_field('phone_1',29)?>"><i class="far fa-phone"></i><?=get_field('phone_1',29)?></a>
                                </li>
                               <!-- <li class="menu-item menu-item-type-post_type menu-item-object-page">
                                    <a href="tel:<?php /*=get_field('phone_1',29)*/?>"><i class="far fa-phone"></i><?php /*=get_field('phone_2',29)*/?></a>
                                </li>-->
                            </ul>
                        </div>
                        <!-- Pushed Item -->
                        <div class="nav-pushed-item"></div>
                    </div>

                    <!-- Navbar Extra  -->
                    <div class="navbar-extra d-lg-block d-flex align-items-center">
                        <!-- Navbtn -->
                        <div class="navbar-btn nav-push-item">
                            <a class="main-btn main-btn-3" href="#modal" data-toggle="modal" data-target="#modal"><?=get_btn_text_by_lang('contact-us')?></a>
                        </div>
                        <!-- Navbar Toggler -->
                        <div class="navbar-toggler">
                            <span></span><span></span><span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
