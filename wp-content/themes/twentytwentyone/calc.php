<?php
/*Template Name: Calc*/
$prices = get_prices_calc($_POST);

$name = 'Ваше имя';
$phone = 'Ваш номер телефона';
$msg ='Сообщение';
$contact_us = 'Обсудить проект';
$url = '/thank-you-ru';
get_header();
?>

<!--====== Contact Section start ======-->
<section class="contact-section contact-page section-gap-top" style="padding-top: 200px">
    <div class="container">
        <div class="contact-info">
            <div class="row justify-content-center">
                <div class="col-lg-6 order-2 order-lg-1">
                    <form action="<?=$url?>" method="post" class="contact-form grey-bg">
                        <div class="row">
                            <div class="col-12">
                                <div class="input-group mb-30">
                                    <input name="tg_name" required type="text" placeholder="<?=$name?>">
                                    <span class="icon"><i class="far fa-user-circle"></i></span>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="input-group mb-30">
                                    <input name="tg_phone" type="text" required  id="phone1" placeholder="<?=$phone?>">
                                    <span class="icon"><i class="far fa-phone"></i></span>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="input-group textarea mb-30">
        <textarea name="tg_msg" placeholder="<?=$msg?>"><?php
            // Вставляем текстовые описания выбранных параметров из POST-запроса, если они были отправлены
            if (isset($_POST['page-count'])) {
                echo "Количество страниц: ";
                switch ($_POST['page-count']) {
                    case '1':
                        echo "1-5";
                        break;
                    case '2':
                        echo "5-15";
                        break;
                    case '3':
                        echo "15-35";
                        break;
                }
                echo "\n";

                echo "Уровень дизайна: ";
                switch ($_POST['design-level']) {
                    case '1':
                        echo "Простой";
                        break;
                    case '2':
                        echo "Высокий";
                        break;
                    case '3':
                        echo "Особый";
                        break;
                }
                echo "\n";

                echo "Мобильная версия: ";
                echo ($_POST['mobile'] == "1" ? "Нет" : "Включена") . "\n";

                echo "CMS: ";
                switch ($_POST['cms']) {
                    case '1':
                        echo "Не нужно";
                        break;
                    case '2':
                        echo "Стандартный";
                        break;
                    case '3':
                        echo "Расширенный";
                        break;
                }
                echo "\n";

                echo "Количество языков: ";
                echo $_POST['lang'] . "\n";

                echo "Написание текста: ";
                switch ($_POST['text']) {
                    case '1':
                        echo "Не нужно";
                        break;
                    case '2':
                        echo "2-5 страниц";
                        break;
                    case '3':
                        echo "5-10 страниц";
                        break;
                }
                echo "\n";

                echo "Услуга SEO: ";
                switch ($_POST['seo']) {
                    case '1':
                        echo "Не нужно";
                        break;
                    case '2':
                        echo "40 слов";
                        break;
                    case '3':
                        echo "90 слов";
                        break;
                }
                echo "\n";
            }
            ?></textarea>
                                    <span class="icon"><i class="far fa-pencil"></i></span>
                                </div>
                            </div>


                            <div class="col-12 text-center">
                                <button type="submit" class="main-btn"><?=get_btn_text_by_lang('send')?></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6 col-md-10 order-1 order-lg-2">
                    <div class="contact-info-content">
                        <div class="section-title left-border mb-40">
                            <span class="title-tag">Расчет</span>
                            <h2 class="title">КАЛЬКУЛЯТОР СТОИМОСТИ СОЗДАНИЯ САЙТА</h2>
                        </div>
                        <p class="contact-p"">
                            Цена вашего будущего сайта: <?php echo $prices?> €
                        </p>

                        <ul>
                            <li class="phone">
                                <a href=""><i class="far fa-phone"></i>+371-27-838-559</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>
<!--====== Contact Section start ======-->

<!--====== Client Area Start ======-->
<section class="client-section">
    <div class="container">
        <div class="client-slider section-gap line-top">
            <div class="row align-items-center justify-content-between" id="clientSlider">
                <div class="col">
                    <a href="#" class="client-img d-block text-center">
                        <img src="assets/img/clients/01.png" alt="">
                    </a>
                </div>
                <div class="col">
                    <a href="#" class="client-img d-block text-center">
                        <img src="assets/img/clients/02.png" alt="">
                    </a>
                </div>
                <div class="col">
                    <a href="#" class="client-img d-block text-center">
                        <img src="assets/img/clients/03.png" alt="">
                    </a>
                </div>
                <div class="col">
                    <a href="#" class="client-img d-block text-center">
                        <img src="assets/img/clients/04.png" alt="">
                    </a>
                </div>
                <div class="col">
                    <a href="#" class="client-img d-block text-center">
                        <img src="assets/img/clients/05.png" alt="">
                    </a>
                </div>
                <div class="col">
                    <a href="#" class="client-img d-block text-center">
                        <img src="assets/img/clients/01.png" alt="">
                    </a>
                </div>
                <div class="col">
                    <a href="#" class="client-img d-block text-center">
                        <img src="assets/img/clients/02.png" alt="">
                    </a>
                </div>
                <div class="col">
                    <a href="#" class="client-img d-block text-center">
                        <img src="assets/img/clients/03.png" alt="">
                    </a>
                </div>
                <div class="col">
                    <a href="#" class="client-img d-block text-center">
                        <img src="assets/img/clients/04.png" alt="">
                    </a>
                </div>
                <div class="col">
                    <a href="#" class="client-img d-block text-center">
                        <img src="assets/img/clients/05.png" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--====== Client Area End ======-->


<?php
get_footer();
?>
