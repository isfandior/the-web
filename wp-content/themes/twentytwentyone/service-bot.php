<?php
/*
Template Name: TG BOT service
Template Post Type: post, services 
*/

get_header(); 

?>
    <!--====== Banner Section start ======-->
    <section class="banner-section banner-section-three">
        <div class="banner-slider">
            <div class="single-banner">
                <div class="container-fluid container-1600">
                    <div class="row align-items-center">
                        <div class="col-md-5">
                            <div class="banner-content">
                                <span class="promo-text wow fadeInLeft" data-wow-duration="1500ms"
                                    data-wow-delay="400ms">business & consulting</span>
                                <h1 class="wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="500ms">
                                    Perfect Agency <br> For Innovative <br> Business
                                </h1>
                                <ul class="btn-wrap">
                                    <li class="wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="600ms">
                                        <a href="#" class="main-btn">Get Started Now</a>
                                    </li>
                                    <li class="wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="700ms">
                                        <a href="#" class="main-btn main-btn-3">Our Services</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-7 d-none d-md-block">
                            <div class="banner-img text-right wow fadeInRight" data-wow-duration="1500ms"
                                data-wow-delay="800ms">
                                <img src="<?=get_template_directory_uri()?>/assets/img/illustration/04.png" alt="illustration">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="banner-shapes">
                    <div class="one"></div>
                    <div class="two"></div>
                    <div class="three"></div>
                </div>
                <div class="banner-line">
                    <img src="<?=get_template_directory_uri()?>/assets/img/lines/17.png" alt="Image">
                </div>
            </div>
        </div>
    </section>
    <!--====== Banner Section end ======-->

    <!--====== Types Of Service Name Section Start ======-->
    <section class="prices-section section-gap">
        <div class="container">
            <div class="prices__header">
                <div class="container section-title">
                    <h2 class="title">Types Of Google Adword </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="prices__block">
                        <h4 class="prices__title">Intro</h4>
                        <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                        <ul class="mt-3 prices__list-parent">
                            <li class="prices__list">All limited links</li>
                            <li class="prices__list">Own analytics platform</li>
                            <li class="prices__list">Chat Support</li>
                            <li class="prices__list">Optimize Hashtags</li>
                            <li class="prices__list">Unlimited Users</li>
                        </ul>
                        <a class="main-btn mt-5" href="#">Order</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="prices__block">
                        <h4 class="prices__title">Base</h4>
                        <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                        <ul class="mt-3 prices__list-parent">
                            <li class="prices__list">All limited links</li>
                            <li class="prices__list">Own analytics platform</li>
                            <li class="prices__list">Chat Support</li>
                            <li class="prices__list">Optimize Hashtags</li>
                            <li class="prices__list">Unlimited Users</li>
                        </ul>
                        <a class="main-btn mt-5" href="#">Order</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="prices__block">
                        <h4 class="prices__title">Popular</h4>
                        <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                        <ul class="mt-3 prices__list-parent">
                            <li class="prices__list">All limited links</li>
                            <li class="prices__list">Own analytics platform</li>
                            <li class="prices__list">Chat Support</li>
                            <li class="prices__list">Optimize Hashtags</li>
                            <li class="prices__list">Unlimited Users</li>
                        </ul>
                        <a class="main-btn mt-5" href="#">Order</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Types OF Service Name Section end ======-->

    <!--====== CTA Start ======-->
    <section class="cta-aection section-gap">
        <div class="container">
            <div class="cta-wrap bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/lines/16.png);">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="cta-content text-center">
                            <div class="section-title both-border mb-30">
                                <span class="title-tag">Get A Quote</span>
                                <h2 class="title">Feel Any Project For Business Consulting Get Started Us</h2>
                            </div>
                            <a href="#" class="main-btn main-btn-3">Get Started</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== CTA End ======-->

    <!--====== Price Section Start ======-->
    <section class="prices-section section-gap">
        <div class="container">
            <div class="prices__header">
                <div class="container section-title">
                    <h2 class="title">Our Prices </h2>
                </div>
                <div id="myBtnContainer">
                    <div class="btn-container">
                      <button class="filter-btn active" onclick="filterSelection('month')"> Monthly</button>
                      <button class="filter-btn" onclick="filterSelection('year')"> Yearly</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 filterDiv month">
                    <div class="prices__block">
                        <h4 class="prices__title">Intro</h4>
                        <h3 class="prices mt-3">$19 <span class="prices__period">/monthly</span></h3>
                        <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                        <ul class="mt-3 prices__list-parent">
                            <li class="prices__list">All limited links</li>
                            <li class="prices__list">Own analytics platform</li>
                            <li class="prices__list">Chat Support</li>
                            <li class="prices__list">Optimize Hashtags</li>
                            <li class="prices__list">Unlimited Users</li>
                        </ul>
                        <a class="main-btn mt-5" href="#">Choose Plan</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 filterDiv month">
                    <div class="prices__block">
                        <h4 class="prices__title">Base</h4>
                        <h3 class="prices mt-3">$39 <span class="prices__period">/monthly</span></h3>
                        <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                        <ul class="mt-3 prices__list-parent">
                            <li class="prices__list">All limited links</li>
                            <li class="prices__list">Own analytics platform</li>
                            <li class="prices__list">Chat Support</li>
                            <li class="prices__list">Optimize Hashtags</li>
                            <li class="prices__list">Unlimited Users</li>
                        </ul>
                        <a class="main-btn mt-5" href="#">Choose Plan</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 filterDiv month">
                    <div class="prices__block">
                        <h4 class="prices__title">Popular</h4>
                        <h3 class="prices mt-3">$99 <span class="prices__period">/monthly</span></h3>
                        <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                        <ul class="mt-3 prices__list-parent">
                            <li class="prices__list">All limited links</li>
                            <li class="prices__list">Own analytics platform</li>
                            <li class="prices__list">Chat Support</li>
                            <li class="prices__list">Optimize Hashtags</li>
                            <li class="prices__list">Unlimited Users</li>
                        </ul>
                        <a class="main-btn mt-5" href="#">Choose Plan</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 filterDiv year">
                    <div class="prices__block">
                        <h4 class="prices__title">Intro</h4>
                        <h3 class="prices mt-3">$19 <span class="prices__period">/yearly</span></h3>
                        <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                        <ul class="mt-3 prices__list-parent">
                            <li class="prices__list">All limited links</li>
                            <li class="prices__list">Own analytics platform</li>
                            <li class="prices__list">Chat Support</li>
                            <li class="prices__list">Optimize Hashtags</li>
                            <li class="prices__list">Unlimited Users</li>
                        </ul>
                        <a class="main-btn mt-5" href="#">Choose Plan</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 filterDiv year">
                    <div class="prices__block">
                        <h4 class="prices__title">Base</h4>
                        <h3 class="prices mt-3">$39 <span class="prices__period">/yearly</span></h3>
                        <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                        <ul class="mt-3 prices__list-parent">
                            <li class="prices__list">All limited links</li>
                            <li class="prices__list">Own analytics platform</li>
                            <li class="prices__list">Chat Support</li>
                            <li class="prices__list">Optimize Hashtags</li>
                            <li class="prices__list">Unlimited Users</li>
                        </ul>
                        <a class="main-btn mt-5" href="#">Choose Plan</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 filterDiv year">
                    <div class="prices__block">
                        <h4 class="prices__title">Popular</h4>
                        <h3 class="prices mt-3">$99 <span class="prices__period">/yearly</span></h3>
                        <p class="mt-3 prices__description">For most businesses that want to optimize web queries</p>
                        <ul class="mt-3 prices__list-parent">
                            <li class="prices__list">All limited links</li>
                            <li class="prices__list">Own analytics platform</li>
                            <li class="prices__list">Chat Support</li>
                            <li class="prices__list">Optimize Hashtags</li>
                            <li class="prices__list">Unlimited Users</li>
                        </ul>
                        <a class="main-btn mt-5" href="#">Choose Plan</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Price Section end ======-->

    <!--====== Service Section Start ======-->
    <section class="service-section grey-bg service-line-shape section-gap">
        <div class="container">
            <!-- Section Title -->
            <div class="section-title text-center both-border mb-50">
                <span class="title-tag">Most Features</span>
                <h2 class="title">We Provide Most Exclusive <br> Service For Business</h2>
            </div>
            <!-- Services Boxes -->
            <div class="row service-boxes justify-content-center">
                <div class="col-lg-3 col-sm-6 col-10 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="800ms">
                    <div class="service-box-three border-0">
                        <div class="icon">
                            <img src="<?=get_template_directory_uri()?>/assets/img/icons/01.png" alt="Icon">
                        </div>
                        <h3><a href="#">Creative Idea</a></h3>
                        <p>Sed perspicia unde omnis</p>
                        <a href="#" class="service-link">
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-10 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="900ms">
                    <div class="service-box-three border-0">
                        <div class="icon">
                            <img src="<?=get_template_directory_uri()?>/assets/img/icons/02.png" alt="Icon">
                        </div>
                        <h3><a href="#">Business Strategy</a></h3>
                        <p>Quis autem velrepres hend</p>
                        <a href="#" class="service-link">
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-10 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="1s">
                    <div class="service-box-three border-0">
                        <div class="icon">
                            <img src="<?=get_template_directory_uri()?>/assets/img/icons/03.png" alt="Icon">
                        </div>
                        <h3><a href="#">Relationship</a></h3>
                        <p>Sed perspicia unde omnis</p>
                        <a href="#" class="service-link">
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-10 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="1.1s">
                    <div class="service-box-three border-0">
                        <div class="icon">
                            <img src="<?=get_template_directory_uri()?>/assets/img/icons/04.png" alt="Icon">
                        </div>
                        <h3><a href="#">Productivity</a></h3>
                        <p>Quis autem velrepres hend</p>
                        <a href="#" class="service-link">
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="line-one">
            <img src="<?=get_template_directory_uri()?>/assets/img/lines/12.png" alt="line-shape">
        </div>
        <div class="line-two">
            <img src="<?=get_template_directory_uri()?>/assets/img/lines/11.png" alt="line-shape">
        </div>
        <div class="working-circle"></div>
    </section>
    <!--====== Service Section End ======-->



<?php
get_footer();