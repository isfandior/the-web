<?php 
$portfolio_items = get_field('portfolio_items');
 ?>
<section class="project-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7 col-md-8">
                    <!-- Section Title -->
                    <div class="section-title left-border">
                        <span class="title-tag"><?=get_field('portfolio_title')?></span>
                        <h2 class="title"><?=get_field('portfolio_subtitle')?></h2>
                    </div>
                </div>
                <!--<div class="col-lg-5 col-md-4">
                    <div class="view-moore-btn text-md-right mt-30 mt-md-0">
                        <a href="#" class="main-btn"><?php /*=get_btn_text_by_lang('all_portfolio')*/?></a>
                    </div>
                </div>-->
            </div>

            <!-- Project Boxes -->
            <div class="row project-boxes mt-80 justify-content-center">
                <?php foreach ($portfolio_items as $portfolio):?>
                <div class="col-lg-4 col-sm-6">
                    <div class="project-box">
                        <div class="project-thumb">
                            <div class="thumb bg-img-c img-portfolio" onclick="goToUrl('<?=get_field('url',$portfolio->ID)?>')" style="background-image: url(<?=wp_get_attachment_url(get_post_thumbnail_id($portfolio->ID), 'thumbnail' );?>);"></div>
                        </div>
                        <div class="project-desc text-center">
                            <h4><a href="<?=get_permalink($portfolio)?>" target="_blank"><?=get_the_title($portfolio)?></a></h4>
                            <p><?=get_field('category',$portfolio->ID)?></p>
                            <a href="<?=get_field('url',$portfolio->ID)?>" target="_blank" class="project-link">
                                <i class="fal fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
    </section>