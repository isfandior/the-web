<section class="wcu-section section-gap">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-lg-6 col-md-10">
                <div class="wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="400ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 400ms; animation-name: fadeInUp;">
                    <img src="<?=get_field('photo_service_about')?>" alt="">
                </div>
            </div>
            <div class="col-lg-6 col-md-10">
                <div class="wcu-text-two">
                    <div class="section-title left-border mb-40">
                        <span class="title-tag"><?=get_field('header_service_about')?></span>
                        <h2 class="title"><?=get_field('big_header_service_about')?></h2>
                    </div>
                    <p>
                        <?=get_field('desc_service_about')?>
                    </p>
                    <?=get_field('content_service_about')?>
                    <a href="#" class="main-btn"><?=get_field('btn_service_about')?></a>
                </div>
            </div>
        </div>
    </div>
</section>