<section class="faq-section section-gap with-illustration with-shape grey-bg">
    <div class="container">
        <div class="faq-illustration-img">
            <img src="<?=get_field('faq_img')?>" alt="illustration">
        </div>
        <div class="row justify-content-lg-end justify-content-center">
            <div class="col-lg-6 col-md-10">
                <div class="faq-content">
                    <div class="section-title mb-40 left-border">
                        <span class="title-tag"><?=get_field('faq_subtitle')?></span>
                        <h2 class="title"><?=get_field('faq_title')?></h2>
                    </div>
                    <!-- FAQ LOOP -->
                    <div class="accordion faq-loop" id="faqAccordion">
                        <?php for ($i=1; $i <5 ; $i++):?>
                        <div class="card">
                            <div class="card-header">
                                <h6 class="collapsed" data-toggle="collapse" data-target="#collapse<?=$i?>">
                                    <?=get_field('faq_question_'.$i)?>
                                    <span class="icons">
                                        <i class="far fa-plus"></i>
                                    </span>
                                </h6>
                            </div>

                            <div id="collapse<?=$i?>" class="collapse" data-parent="#faqAccordion">
                                <div class="card-body">
                                    <?=get_field('faq_answer_'.$i)?>
                                </div>
                            </div>
                        </div>
                        <?php endfor?>
                    </div>
                    <!-- End Faq LOOP -->
                </div>
            </div>
        </div>
        <div class="circle-img" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/faq.svg);"></div>
    </div>
</section>