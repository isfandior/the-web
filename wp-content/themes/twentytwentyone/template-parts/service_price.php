<section class="prices-section pb-5" id="price">
    <div class="container">
        <div class="prices__header">
            <div class="container section-title">
                <h2 class="title"><?=get_field('price_title')?></h2>
            </div>
        </div>
        <div class="row">
            <?php for ($i=1; $i < 4 ; $i++): ?>
                <div class="col-md-4 col-sm-6 filterDiv month">
                    <div class="prices__block">
                        <h4 class="prices__title"><?=get_field('price_name_'.$i)?> </h4>
                        <h3 class="prices mt-3"><i class="fas fa-euro-sign price_currency"></i> <?=get_field('price_'.$i)?> </h3>
                        <?=get_field('price_content_'.$i)?>
                        <a class="main-btn mt-5" href="#modal" data-toggle="modal" data-target="#modal"><?=get_btn_text_by_lang('order')?></a>
                    </div>
                </div>
            <?php endfor?>
        </div>
    </div>
</section>