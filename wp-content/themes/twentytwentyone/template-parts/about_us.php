<section class="about-section-three section-gap">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-10 order-2 order-lg-1">
                <div class="about-text-three">
                    <div class="section-title left-border mb-40">
                        <span class="title-tag"><?=get_field('about_section_subtitle')?></span>
                        <h2 class="title"><?=get_field('about_section_title')?> </h2>
                    </div>
                    <?=get_field('about_desc')?>
                </div>
            </div>
            <div class="col-lg-6 col-md-10 order-1 order-lg-2">
                <div class="about-tile-gallery">
                    <img src="<?=get_field('about_img')?>" alt="Image" class="image-one wow fadeInRight"
                    data-wow-duration="1500ms" data-wow-delay="400ms">
                </div>
            </div>
        </div>
    </div>
</section>