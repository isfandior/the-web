<section class="cta-aection section-gap">
    <div class="container">
        <div class="cta-wrap bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/lines/16.png);">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="cta-content text-center">
                        <div class="section-title both-border mb-30">
                            <span class="title-tag"><?=get_field('cta_title', 16)?></span>
                            <h2 class="title"><?=get_field('cta_text', 16)?></h2>
                        </div>
                        <a href="#modal" data-toggle="modal" data-target="#modal" class="main-btn main-btn-3"><?=get_btn_text_by_lang('start_project')?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>