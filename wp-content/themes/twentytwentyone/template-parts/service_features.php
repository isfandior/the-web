<section class="service-section shape-style-one section-gap grey-bg mb-5">
    <div class="container">
        <!-- Section Title -->
        <div class="section-title text-center both-border mb-30">
            <span class="title-tag"><?=get_field('header_tag_service_features')?></span>
            <h2 class="title"><?=get_field('header_service_features')?></h2>
        </div>
        <!-- Services Boxes -->
        <div class="row service-boxes justify-content-center">
            <div class="col-lg-4 col-md-6 col-sm-8 col-10 col-tiny-12 wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="400ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 400ms; animation-name: fadeInLeft;">
                <div class="service-box text-center">
                    <div class="icon">
                        <img src="<?=get_field('service_feature_icon_1')?>" alt="Icon">
                    </div>
                    <h3><?=get_field('service_feature_header_1')?></h3>
                    <?=get_field('service_feature_content_1')?>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-8 col-10 col-tiny-12 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="600ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 600ms; animation-name: fadeInUp;">
                <div class="service-box text-center">
                    <div class="icon">
                        <img src="<?=get_field('service_feature_icon_2')?>" alt="Icon">
                    </div>
                    <h3><?=get_field('service_feature_header_2')?></h3>
                    <?=get_field('service_feature_content_2')?>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-8 col-10 col-tiny-12 wow fadeInRight" data-wow-duration="1500ms" data-wow-delay="800ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 800ms; animation-name: fadeInRight;">
                <div class="service-box text-center">
                    <div class="icon">
                        <img src="<?=get_field('service_feature_icon_3')?>" alt="Icon">
                    </div>
                    <h3><?=get_field('service_feature_header_3')?></h3>
                    <?=get_field('service_feature_content_3')?>
                </div>
            </div>
        </div>
    </div>
    <div class="dots-line">
        <img src="<?=get_template_directory_uri()?>/assets/img/lines/07.png" alt="Image">
    </div>
</section>