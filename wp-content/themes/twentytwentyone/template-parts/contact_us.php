<?php 
$lang = pll_current_language();
if ($lang == 'ru') {
    $name = 'Ваше имя';
    $phone = 'Ваш номер телефона';
    $msg ='Сообщение';
    $url = '/thank-you-ru';
}else{
    $name = 'Ismingiz';
    $phone = 'Telefongiz';
    $msg ='Xabar';
    $url = '/uz/thank-you';
}

 ?>
<section class="contact-section boxed-style-with-map">
        <div class="container">
            <div class="contact-inner mt-15 mb-15 grey-bg">
                <div class="row no-gutters">
                    <div class="col-lg-12">
                        <div class="contact-form">
                            <div class="section-title left-border mb-30">
                                <span class="title-tag"><?=get_field('contact_title')?></span>
                                <h2 class="title"><?=get_field('contact_subtitle')?></h2>
                            </div>

                            <form action="<?=$url?>" method='post'>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="input-group mb-30">
                                            <input type="text" name="tg_name" placeholder="<?=$name?>">
                                            <span class="icon"><i class="far fa-user-circle"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="input-group mb-30">
                                            <input type="text" name="tg_phone" id="phone2" placeholder="<?=$phone?>">
                                            <span class="icon"><i class="far fa-phone"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="input-group textarea mb-30">
                                            <textarea name="tg_msg" placeholder="<?=$msg?>"></textarea>
                                            <span class="icon"><i class="far fa-pencil"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="main-btn"><?=get_btn_text_by_lang('send_msg')?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

