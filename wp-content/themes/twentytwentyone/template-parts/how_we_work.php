<section class="working-process-section">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-lg-6 col-md-10 order-lg-1 order-2">
                <div class="process-text">
                    <!-- Section Title -->
                    <div class="section-title left-border mb-30">
                        <span class="title-tag"><?=get_field('working_subtitle')?></span>
                        <h2 class="title"><?=get_field('working_title')?></h2>
                    </div>
                    <p>
                        <?=get_field('working_text')?>
                    </p>
                    <!-- process-loop -->
                    <div class="process-loop">
                        <?php for ($i=1; $i < 4; $i++):?>
                            <div class="single-process wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="400ms">
                                <div class="icon">
                                    <i class="fal fa-coffee"></i>
                                    <span><?=$i?></span>
                                </div>
                                <div class="content">
                                    <h4><?=get_field('working_step_title_' . $i)?></h4>
                                    <p><?=get_field('working_step_text_' . $i)?></p>
                                </div>
                            </div>
                        <?php endfor?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-10 order-lg-2 order-1">
                <img src="<?=get_field('working_img')?>">
            </div>
        </div>
    </div>
</section>