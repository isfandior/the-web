<section class="wcu-section box-style mb-80">
    <div class="container">
        <div class="wcu-inner">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-6">
                    <div class="wcu-image text-center text-lg-left wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="400ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 400ms; animation-name: fadeInUp;">
                        <img src="<?=get_field('image_why_us')?>" alt="Image">
                    </div>
                </div>
                <div class="col-lg-6 col-md-10">
                    <div class="wcu-text">
                        <div class="section-title left-border mb-40">
                            <span class="title-tag"><?=get_field('header_tag_why_us')?></span>
                            <h2 class="title"><?=get_field('header_why_us')?></h2>
                        </div>
                        <?=get_field('content_why_us')?>

                        <a href="<?=get_field('btn_url_why_us')?>" class="main-btn main-btn-4"><?=get_field('btn_why_us')?></a>
                    </div>
                </div>
            </div>
            <img src="<?=get_template_directory_uri()?>/assets/img/lines/03.png" alt="shape" class="line-shape-one">
            <img src="<?=get_template_directory_uri()?>/assets/img/lines/04.png" alt="shape" class="line-shape-two">
        </div>
    </div>
</section>