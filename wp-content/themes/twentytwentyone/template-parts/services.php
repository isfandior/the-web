<?php 
$services = get_field('services');
$lang = pll_current_language();
if ($lang == 'ru') {
    $service_url = get_permalink('122');    
}else{
    $service_url = get_permalink('124');
}

 ?>
<section class="service-section section-gap">
    <div class="container text-center">
        <div class="section-title text-center both-border mb-50">
            <span class="title-tag"><?=get_field('services_title')?></span>
            <h2 class="title"><?=get_field('services_subtitle')?></h2>
        </div>
        <div class="row service-boxes justify-content-center">
            <?php foreach ($services as $service): ?>
            <div class="col-lg-3 col-sm-6 col-10 wow fadeInLeft" data-wow-duration="1500ms" data-wow-delay="400ms">
                <div class="service-box-three">
                    <div class="icon">
                        <img src="<?=get_field('service_icon',$service->ID)?>" alt="Icon">
                    </div>
                    <h3>
                        <a href="<?=get_permalink($service->ID)?>"><?=$service->post_title?></a>
                    </h3>
                    <p><?=get_field('short_description',$service->ID)?></p>
                    <a href="<?=get_permalink($service->ID)?>" class="service-link">
                        <i class="fal fa-long-arrow-right"></i>
                    </a>
                </div>
            </div>
            <?php endforeach ?>
        </div>
        <a href="<?=$service_url?>" class="main-btn mt-5">
            <?=get_btn_text_by_lang('other_services')?>
        </a>
    </div>
</section>