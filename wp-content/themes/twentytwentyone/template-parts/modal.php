<?php 
$lang = pll_current_language();
if ($lang == 'ru') {
    $name = 'Ваше имя';
    $phone = 'Ваш номер телефона';
    $msg ='Сообщение';
    $contact_us = 'Обсудить проект';
    $url = '/thank-you-ru';
}else{
    $name = 'Ismingiz';
    $phone = 'Telefongiz';
    $msg ='Xabar';
    $contact_us = "Mutaxassis bilan bog'lanish";
    $url = '/uz/thank-you';
}


 ?>

<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" aria-labelledby="exampleModalLabel"aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?=$contact_us?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?=$url?>" method="post">
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group mb-30">
                                <input name="tg_name" required type="text" placeholder="<?=$name?>">
                                <span class="icon"><i class="far fa-user-circle"></i></span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="input-group mb-30">
                                <input name="tg_phone" type="text" required  id="phone1" placeholder="<?=$phone?>">
                                <span class="icon"><i class="far fa-phone"></i></span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="input-group textarea mb-30">
                                <textarea name="tg_msg" placeholder="<?=$msg?>"></textarea>
                                <span class="icon"><i class="far fa-pencil"></i></span>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="main-btn"><?=get_btn_text_by_lang('send')?></button>
                        </div>
                    </div>
                </form>   
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var element = document.getElementById('phone1');
    var maskOptions = {
    mask: '+{371}-00-000-000'
    };
    var mask = IMask(element, maskOptions);
    
    var element2 = document.getElementById('phone2');
    var mask2 = IMask(element2, maskOptions);


</script>