<div class="container calc-block mt-5 mb-5" style="position: relative">
    <form action="/calc" method="post">
    <div class="row">
        <div class="col-md-12">
            <h3>Рассчитать стоимость</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Количество страниц
        </div>
        <div class="col-md-6">
            <div class="radios">
                <input id="page-count-1" name="page-count" type="radio" value="1">
                <label for="page-count-1">1-5</label>

                <input id="page-count-2" name="page-count" type="radio" value="2" checked>
                <label for="page-count-2">5-15</label>

                <input id="page-count-3" name="page-count" type="radio" value="3">
                <label for="page-count-3">15-35</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Уровень дизайна
        </div>
        <div class="col-md-6">
            <div class="radios">
                <input id="design-level-1" name="design-level" type="radio" value="1">
                <label for="design-level-1">Простой</label>

                <input id="design-level-2" name="design-level" type="radio" value="2" checked>
                <label for="design-level-2">Высокий</label>

                <input id="design-level-3" name="design-level" type="radio" value="3">
                <label for="design-level-3">Особый</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Мобильная версия
        </div>
        <div class="col-md-6">
            <div class="radios">
                <input disabled id="mobile-1" name="mobile" type="radio" value="1">
                <label for="mobile-1">Нет</label>

                <input disabled id="mobile-2" name="mobile" type="radio" value="2" checked>
                <label for="mobile-2">Включена</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Система управления сайтом <br> <span class="calc-span">(CMS)</span>
        </div>
        <div class="col-md-6">
            <div class="radios">
                <input id="cms-1" name="cms" type="radio" value="1">
                <label for="cms-1">Не нужно</label>

                <input id="cms-2" name="cms" type="radio" value="2" checked>
                <label for="cms-2">Стандартный</label>

                <input id="cms-3" name="cms" type="radio" value="3">
                <label for="cms-3">Расширенный</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Количество языков <br><span class="calc-span">(Латышский, Русский, Английский)</span>
        </div>
        <div class="col-md-6">
            <div class="radios">
                <input id="lang-1" name="lang" type="radio" value="1">
                <label for="lang-1">1</label>

                <input id="lang-2" name="lang" type="radio" value="2" checked>
                <label for="lang-2">2</label>

                <input id="lang-3" name="lang" type="radio" value="3">
                <label for="lang-3">3</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Написание текста <br><span class="calc-span">(Услуги копирайтера)</span>
        </div>
        <div class="col-md-6">
            <div class="radios">
                <input id="text-1" name="text" type="radio" value="1">
                <label for="text-1">Не нужно</label>

                <input id="text-2" name="text" type="radio" value="2" checked>
                <label for="text-2">2-5 страниц</label>

                <input id="text-3" name="text" type="radio" value="3">
                <label for="text-3">5-10 страниц</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Услуга SEO <br><span class="calc-span">(Поднятие позиция сайта в Google)</span>
        </div>
        <div class="col-md-6">
            <div class="radios">
                <input id="seo-1" name="seo" type="radio" value="1">
                <label for="seo-1">Не нужно</label>

                <input id="seo-2" name="seo" type="radio" value="2" checked>
                <label for="seo-2">40 слов</label>

                <input id="seo-3" name="seo" type="radio" value="3">
                <label for="seo-3">90 слов</label>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-right">
        <button class="main-btn main-btn-3">Рассчитать стоимость</button>
    </div>
    </form>
</div>

