<?php

get_header(); 
$services = get_field('services');

?>
<section id="service-section" class="service-section grey-bg service-line-shape section-gap">
    <div class="container">
        <div class="section-title text-center both-border mb-50">
            <span class="title-tag"><?=get_field('services_subtitle')?></span>
            <h2 class="title"><?=get_field('services_title')?></h2>
        </div>
        <div class="row service-boxes justify-content-center">
            <?php foreach ($services as $service): ?>
            <div class="col-lg-3 col-sm-6 col-10 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="400ms">
                <div class="service-box-three border-0">
                    <div class="icon">
                        <img src="<?=get_field('service_icon',$service->ID)?>" alt="Icon">
                    </div>
                    <h3><a href="<?=get_permalink($service->ID)?>"><?=$service->post_title?></a></h3>
                    <p><?=get_field('short_description',$service->ID)?></p>
                    <a href="<?=get_permalink($service->ID)?>" class="service-link">
                        <i class="fal fa-long-arrow-right"></i>
                    </a>
                </div>
            </div>
            <?php endforeach ?>
        </div>
    </div>
    <div class="line-two">
        <img src="<?=get_template_directory_uri()?>/assets/img/lines/11.png" alt="line-shape">
    </div>
    <div class="working-circle"></div>
</section>