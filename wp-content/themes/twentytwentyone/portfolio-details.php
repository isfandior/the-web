<?php
/*Template Name: Portfolio Details Page*/

get_header(); 

?>
    <!--====== Breadcumb Section start ======-->
    <section class="banner-section banner-section-three">
        <div class="banner-slider">
            <div class="single-banner">
                <div class="container-fluid container-1600">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <h2 class="wow fadeInLeft breadcumb-title">Portfolio Details</h2>
                            <ul class="breadcumb-list">
                                <li class="breadcumb-list_item"><a href="/">Home</a></li>
                                <li class="breadcumb-list_item">Portfolio Details</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="banner-shapes">
                    <div class="one"></div>
                    <div class="two"></div>
                    <div class="three"></div>
                </div>
                <div class="banner-line">
                    <img src="<?=get_template_directory_uri()?>/assets/img/lines/17.png" alt="Image">
                </div>
            </div>
        </div>
    </section>
    <!--====== Breadcumb Section end ======-->
    <!--====== Project Details Start ======-->
    <section class="project-details section-gap">
        <div class="container">
            <div class="project-thumb">
                <img src="<?=get_template_directory_uri()?>/assets/img/details/project-big-img.jpg" alt="Image">
            </div>

            <div class="project-content mt-60">
                <div class="row">
                    <div class="col-lg-8 order-2 order-lg-2">
                        <div class="content">
                            <h2>Business Strategy</h2>

                            <p class="mb-40">
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                                architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas
                                sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
                                voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit
                                amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut
                                labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis
                                nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi
                                consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam
                                nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatu
                            </p>

                            <p>
                                Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci
                                velit, sed quia numquam eius modi tempora incidunt ut labore et dolore magnam aliquam
                                quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis
                                suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure
                                reprehenderit qui in ea voluptate velit esse quam nihil molestiae
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-4 order-1 order-lg-2">
                        <div class="details">
                            <ul>
                                <li>
                                    <h3>Project Name</h3>
                                    <p>Business Strategy</p>
                                </li>
                                <li>
                                    <h3>Clients Name</h3>
                                    <p>Logan Parkinson</p>
                                </li>
                                <li>
                                    <h3>Project Date</h3>
                                    <p>25 Aug 2020</p>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-12 order-3">
                        <div class="thumbs">
                            <div class="row justify-content-center">
                                <div class="col-md-4 col-sm-6 text-center">
                                    <img src="<?=get_template_directory_uri()?>/assets/img/details/project-01.jpg" alt="">
                                </div>
                                <div class="col-md-4 col-sm-6 text-center">
                                    <img src="<?=get_template_directory_uri()?>/assets/img/details/project-02.jpg" alt="">
                                </div>
                                <div class="col-md-4 col-sm-6 text-center">
                                    <img src="<?=get_template_directory_uri()?>/assets/img/details/project-03.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 order-4">
                        <div class="content">
                            <p>
                                But I must explain to you how all this mistaken idea of denouncing pleasure and praising
                                pain was born and I will give you a complete account of the system, and expound the
                                actual teachings of the great explorer of the truth, the master-builder of human
                                happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure,
                                but because those who do not know how to pursue pleasure rationally encounter
                                consequences that are extremely painful. Nor again is there anyone who loves or pursues
                                or desires to obtain pain of itself, because it is pain, but because occasionally
                                circumstances occur in which toil and pain can procure him some great pleasure. To take
                                a trivial example, which of us ever undertakes laborious physical exercise, except to
                                obtain some advantage from it? But who has any right to find fault with a man who
                                chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain
                                that produces no resultant pleasure
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Project Details End ======-->

    <!--====== Related Service Section Start ======-->
    <section class="service-section grey-bg service-line-shape section-gap">
        <div class="container">
            <!-- Section Title -->
            <div class="section-title text-center both-border mb-50">
                <span class="title-tag">Most Features</span>
                <h2 class="title">We Provide Most Exclusive <br> Service For Business</h2>
            </div>
            <!-- Services Boxes -->
            <div class="row service-boxes justify-content-center">
                <div class="col-lg-3 col-sm-6 col-10 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="800ms">
                    <div class="service-box-three border-0">
                        <div class="icon">
                            <img src="<?=get_template_directory_uri()?>/assets/img/icons/01.png" alt="Icon">
                        </div>
                        <h3><a href="#">Creative Idea</a></h3>
                        <p>Sed perspicia unde omnis</p>
                        <a href="#" class="service-link">
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-10 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="900ms">
                    <div class="service-box-three border-0">
                        <div class="icon">
                            <img src="<?=get_template_directory_uri()?>/assets/img/icons/02.png" alt="Icon">
                        </div>
                        <h3><a href="#">Business Strategy</a></h3>
                        <p>Quis autem velrepres hend</p>
                        <a href="#" class="service-link">
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-10 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="1s">
                    <div class="service-box-three border-0">
                        <div class="icon">
                            <img src="<?=get_template_directory_uri()?>/assets/img/icons/03.png" alt="Icon">
                        </div>
                        <h3><a href="#">Relationship</a></h3>
                        <p>Sed perspicia unde omnis</p>
                        <a href="#" class="service-link">
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-10 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="1.1s">
                    <div class="service-box-three border-0">
                        <div class="icon">
                            <img src="<?=get_template_directory_uri()?>/assets/img/icons/04.png" alt="Icon">
                        </div>
                        <h3><a href="#">Productivity</a></h3>
                        <p>Quis autem velrepres hend</p>
                        <a href="#" class="service-link">
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="line-one">
            <img src="<?=get_template_directory_uri()?>/assets/img/lines/12.png" alt="line-shape">
        </div>
        <div class="line-two">
            <img src="<?=get_template_directory_uri()?>/assets/img/lines/11.png" alt="line-shape">
        </div>
        <div class="working-circle"></div>
    </section>
    <!--====== Related Service Section End ======-->

    <!--====== CTA Start ======-->
    <section class="cta-aection section-gap">
        <div class="container">
            <div class="cta-wrap bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/lines/16.png);">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="cta-content text-center">
                            <div class="section-title both-border mb-30">
                                <span class="title-tag">Get A Quote</span>
                                <h2 class="title">Feel Any Project For Business Consulting Get Started Us</h2>
                            </div>
                            <a href="#" class="main-btn main-btn-3">Get Started</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== CTA Start ======-->


    <!--====== Project section Start ======-->
    <section class="project-section">
        <div class="container">
            <div class="col-12">
                <!-- Section Title -->
                <div class="section-title text-center">
                    <span class="title-tag">Latest Project</span>
                    <h2 class="title">We Complate Much More<br>Latest Project</h2>
                </div>
            </div>
            <!-- Project Boxes -->
            <div class="row project-boxes project-isotope mt-60 justify-content-center">
                <div class="isotope-item col-lg-4 col-sm-6 item-one item-four">
                    <div class="project-box hover-style">
                        <div class="project-thumb">
                            <div class="thumb bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/project/05.jpg);"></div>
                        </div>
                        <div class="project-desc text-center">
                            <h4><a href="#">Free Consulting</a></h4>
                            <p>Join us for consultatoins</p>
                            <a href="#" class="project-link">
                                <i class="fal fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="isotope-item col-lg-4 col-sm-6 item-six item-three item-two">
                    <div class="project-box hover-style">
                        <div class="project-thumb">
                            <div class="thumb bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/project/03.jpg);"></div>
                        </div>
                        <div class="project-desc text-center">
                            <h4><a href="#">How To Business</a></h4>
                            <p>Join us for consultatoins</p>
                            <a href="#" class="project-link">
                                <i class="fal fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="isotope-item col-lg-4 col-sm-6 item-two item-five item-one">
                    <div class="project-box hover-style">
                        <div class="project-thumb">
                            <div class="thumb bg-img-c" style="background-image: url(<?=get_template_directory_uri()?>/assets/img/project/14.jpg);"></div>
                        </div>
                        <div class="project-desc text-center">
                            <h4><a href="#">Design Strategy</a></h4>
                            <p>Join us for consultatoins</p>
                            <a href="#" class="project-link">
                                <i class="fal fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Project section End ======-->

<?php
get_footer();