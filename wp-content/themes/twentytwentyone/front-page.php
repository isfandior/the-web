<?php
/*Template Name: Front Page*/

get_header(); 
require('template-parts/banner.php');
require('template-parts/services.php');
require('template-parts/about_us.php');
require('template-parts/portfolio.php');
require('template-parts/cta.php');
require('template-parts/faq.php');
require('template-parts/contact_us.php');

get_footer();
